var apiService = function ($http, authService, configService, $q) {
    return {
        getWorkouts: getWorkouts,
        getRoutes:getRoutes,
        getRouteDetail:getRouteDetail,
        getUserInfo:getUserInfo
    };
    
    function getWorkouts() {
        return $http({
                method: 'GET',
                headers: {
                    'Api-Key': configService.apiKey,
                    'Authorization': 'Bearer ' + configService.accessToken,
                    'Content-Type':'application/json'
                },
                url: configService.baseUrl + 'workout/?user=' + authService.userInfo.id
            })
            .then(getWorkoutsComplete)
            .catch(getWorkoutsFailed);

        function getWorkoutsComplete(response) {
            return response.data._embedded.workouts;
        }

        function getWorkoutsFailed(error) {
        }
    }
    
    function getRoutes() {
        return $http({
                method: 'GET',
                headers: {
                    'Api-Key': configService.apiKey,
                    'Authorization': 'Bearer ' + configService.accessToken,
                    'Content-Type':'application/json'
                },
                url: configService.baseUrl + 'route/?user=' + authService.userInfo.id
            })
            .then(getRoutesComplete)
            .catch(getRoutesFailed);

        function getRoutesComplete(response) {
            return response.data._embedded.routes;
        }

        function getRoutesFailed(error) {
        }
    }
    
    function getRouteDetail(routeId) {
        return $http({
                method: 'GET',
                headers: {
                    'Api-Key': configService.apiKey,
                    'Authorization': 'Bearer ' + configService.accessToken,
                    'Content-Type':'application/json'
                },
                url: configService.baseUrl + 'route/' + routeId + '?field_set=detailed'
            })
            .then(getRouteDetailComplete)
            .catch(getRouteDetailFailed);

        function getRouteDetailComplete(response) {
            return response.data;
        }

        function getRouteDetailFailed(error) {
        }
    }
    
    function getUserInfo() {
        return $http({
                            method: 'GET',
                            headers: {
                                'Api-Key': configService.apiKey,
                                'Authorization': 'Bearer ' + configService.accessToken
                            },
                            url: configService.baseUrl + 'user/self/'
                        })
            .then(getUserInfoComplete)
            .catch(getUserInfoFailed);

        function getUserInfoComplete(response) {
            return response.data;
        }

        function getUserInfoFailed(error) {
        }
    }
}
apiService.$inject = ['$http', 'authService', 'configService', '$q'];
TrailMapper.service('apiService', apiService);