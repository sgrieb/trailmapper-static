



var LoginController = function LoginController($scope, $http, $stateParams, authService, $state, apiService, $cookies) {

    //auth state
    if($state.params.accessToken){
        //successful login
        $cookies.put('trailmapper_ua', $state.params.accessToken);
        $state.go('home');
    }
    
    $scope.onLoginClick = function () {
        window.location = 'https://www.mapmyfitness.com/v7.1/oauth2/authorize/?client_id=zzkvwnz5mmns72pdu8cvx9s7ybuzb4z2&response_type=code&redirect_uri=http%3A%2F%2Flocalhost%3A12345%2Fcallback';
    }
    
}

LoginController.$inject = ['$scope', '$http', '$stateParams', 'authService', '$state', 'apiService', '$cookies'];
TrailMapper.controller('LoginController', LoginController);
