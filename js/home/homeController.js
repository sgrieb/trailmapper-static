
var HomeController = function HomeController($scope, $http, $stateParams, authService, apiService, $state) {

    $scope.sidebarState = '';
    $scope.sidebarIcon = 'glyphicon-chevron-left';
    
    apiService.getUserInfo().then(function(result){
        authService.userInfo = result;
        $scope.userName = result.first_name;
        
        loadMap();
    });
    
    
    function loadMap() {
        var map;
                require(["esri/map", 
                    "esri/geometry/Polyline",
                    "esri/graphic",
                    "esri/symbols/SimpleLineSymbol",
                    "esri/Color",
                    "esri/geometry/Point",
                    "esri/graphicsUtils",
                    "dojo/domReady!"], function (Map , Polyline, Graphic, SimpleLineSymbol, Color, Point, graphicsUtils) {

                    //start up map stuff
                    map = new Map("mapDiv", {
                        basemap: "osm"
                    });

                    map.on("load", function (layer) {
                        
                        //get routes
                        apiService.getRoutes().then(function(result){
                            
                            //got our routes
                            $scope.routes = result;
                            
                            $scope.routeDetails = [];
                            
                            $scope.graphics = [];
                            
                            //go through them
                            $scope.routes.forEach(function(route){
                                if(route._links.self[0]){
                                    
                                    apiService.getRouteDetail(route._links.self[0].id).then(function(result){
                                        
                                        var linePoints = [];
                                        result.points.forEach(function(point){
                                            linePoints.push([point.lng, point.lat]);
                                        });
                                        
                                        var sls = new SimpleLineSymbol(
                                            SimpleLineSymbol.STYLE_DASH,
                                            new Color([255,0,0]),
                                            3
                                        );
                    
                                        var polylineJson = {
                                            "paths":[
                                                linePoints
                                            ],
                                            "spatialReference":{"wkid":4326}
                                        };

                                        var polyline = new Polyline(polylineJson);
                                        
                                        var graphic = new Graphic(polyline, sls);
                                        
                                        map.graphics.add(graphic);
                                        $scope.graphics.push(graphic);
                                        
                                        if(map.graphics.graphics.length == $scope.routes.length){
                                            //all our routes are on the map!
                                            var extent = graphicsUtils.graphicsExtent($scope.graphics);
                                            map.setExtent(extent, true);    
                                        }

                                    });                               
                                }
                            });
                        });

                    });
                });
    }
    
    $scope.toggleSidebar = function(){
        if($scope.sidebarState == ''){
            $scope.sidebarState = 'toggled';
            $scope.sidebarIcon = 'glyphicon-chevron-right';
        }
        else{
            $scope.sidebarState = '';
            $scope.sidebarIcon = 'glyphicon-chevron-left';
        }
        
        $('.sidebarBtn').blur();
    };
}

HomeController.$inject = ['$scope', '$http', '$stateParams', 'authService', 'apiService', '$state'];
TrailMapper.controller('HomeController', HomeController);